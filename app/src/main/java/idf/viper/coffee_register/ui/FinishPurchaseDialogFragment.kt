package idf.viper.coffee_register.ui

import android.app.Dialog
import android.content.ClipData
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.view.animation.AccelerateInterpolator
import android.widget.ImageView
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.view.ContextThemeWrapper
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.ViewModelProviders
import idf.viper.coffee_register.R
import idf.viper.coffee_register.viewmodel.CustomerCartViewModel
import kotlinx.android.synthetic.main.fragment_finish_purchase_dialog.view.*
import com.plattysoft.leonids.ParticleSystem


class FinishPurchaseDialogFragment : DialogFragment() {

    override fun onStart() {
        super.onStart()

        dialog?.window?.setWindowAnimations(
            R.style.DialogAnimation
        )
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {

        val customersVM = activity?.run {
            ViewModelProviders.of(this).get(CustomerCartViewModel::class.java)
        }

        val capsulesAmount = customersVM?.getCapsulesAmount()
        val view = activity!!.layoutInflater.inflate(R.layout.fragment_finish_purchase_dialog, null, false)



        return activity?.let {
            val builder = AlertDialog.Builder(it, R.style.DialogStyle)

            if (capsulesAmount == 0) {
                view.gif_for_coffee.setImageResource(R.drawable.no_capsules_gif)
                builder
                    .setMessage("יש מצב שקצת התבלבלת , איפה הקפסולות ?")
                    .setNeutralButton("אופסי...") { dialog, id ->
                        dialog.cancel()
                    }

            } else {
                view.gif_for_coffee.setImageResource(R.drawable.finish_purchase_gif)
                builder
                    .setMessage(" האם את/ה מוכנ/ה לשתות $capsulesAmount כוסות קפה? ")
                    .setPositiveButton("יאללה לקפה!") { dialog, id ->
                        customersVM?.finishPurchase()
                    }
                    .setNegativeButton("עוד רגע..") { dialog, id ->
                        dialog.cancel()
                    }
            }

            builder
                .setView(view)
            builder.create()
        } ?: throw IllegalStateException("Activity cannot be null")
    }
}