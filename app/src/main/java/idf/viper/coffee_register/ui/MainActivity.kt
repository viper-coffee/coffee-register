package idf.viper.coffee_register.ui

import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.animation.AccelerateInterpolator
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.NavHostFragment
import com.plattysoft.leonids.ParticleSystem
import idf.viper.coffee_register.R
import idf.viper.coffee_register.viewmodel.CustomerCartViewModel

class MainActivity : AppCompatActivity() {

    var customersCartVM: CustomerCartViewModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val host = NavHostFragment.create(R.navigation.nav_graph)
        supportFragmentManager.beginTransaction().replace(R.id.nav_host_fragment, host)
            .setPrimaryNavigationFragment(host).commit()

        customersCartVM =
            ViewModelProviders.of(this).get(CustomerCartViewModel::class.java)

    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater: MenuInflater = menuInflater
        inflater.inflate(R.menu.menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.finish_purchase -> {

                customersCartVM?.let {
                    if (it.getCapsulesAmount() > 0) {
                        ParticleSystem(this, 500, R.drawable.coffee_bean, 6500)
                            .setScaleRange(0.5f, 1.8f)
                            .setSpeedRange(0.1f, 0.25f)
                            .setAcceleration(0.0001f, 45)
                            .setRotationSpeedRange(90f, 180f)
                            .setFadeOut(100, AccelerateInterpolator())
                            .oneShot(findViewById(R.id.finish_purchase), 200)
                    }
                }

                val fragment = FinishPurchaseDialogFragment()
                fragment.show(supportFragmentManager, "dialog")
            }
        }
        return super.onOptionsItemSelected(item)
    }
}