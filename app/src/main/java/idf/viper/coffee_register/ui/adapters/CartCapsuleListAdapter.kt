package idf.viper.coffee_register.ui.adapters

import android.view.*
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import idf.viper.coffee_register.R
import idf.viper.coffee_register.model.Capsule
import idf.viper.coffee_register.model.SelectedCustomer
import kotlinx.android.synthetic.main.capsule_card.view.*

class CartCapsuleListAdapter(private val selectedCustomer: SelectedCustomer):
    androidx.recyclerview.widget.ListAdapter<Capsule, CartCapsuleListAdapter.CartCapsuleViewHolder>(DiffCartCapsuleCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CartCapsuleViewHolder {
        return CartCapsuleViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.capsule_card, parent, false)
        )
    }

    override fun onBindViewHolder(holder: CartCapsuleViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    inner class CartCapsuleViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        fun bind(selectedCap: Capsule) {
            itemView.txt_name.text = selectedCap.name
            itemView.txt_price.text = itemView.context.getString(R.string.shekel_sign).plus(selectedCap.price)
            itemView.txt_intensity.text = selectedCap.intensity.toString()

            if (selectedCap.itemPic == null) {
                itemView.img_capsule.setImageDrawable(ContextCompat.getDrawable(itemView.context, R.mipmap.ic_launcher))
            } else {
                itemView.img_capsule.setImageBitmap(selectedCap.itemPic)
            }

            selectedCap.brandPic?.let { itemView.img_brand.setImageBitmap(it) }

            itemView.apply {
                scaleX = 0.75f
                scaleY = 0.75f

                setOnClickListener{
                    selectedCustomer.purchase.capsules.remove(selectedCap)
                    selectedCustomer.purchase.quantity = selectedCustomer.purchase.capsules.size
                    selectedCustomer.purchase.price = selectedCustomer.purchase.capsules.sumByDouble { it.price }

                    submitList(selectedCustomer.purchase.capsules.toList())
                    itemView.setOnClickListener(null)
                }
            }
        }
    }
}

private class DiffCartCapsuleCallback : DiffUtil.ItemCallback<Capsule>() {
    override fun areItemsTheSame(oldItem: Capsule, newItem: Capsule): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: Capsule, newItem: Capsule): Boolean {
        return oldItem == newItem
    }
}