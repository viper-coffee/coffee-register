package idf.viper.coffee_register.ui.adapters

import android.content.ClipData
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DiffUtil
import idf.viper.coffee_register.R
import idf.viper.coffee_register.model.StockItem
import kotlinx.android.synthetic.main.capsule_card.view.*

open class StoreItemsListAdapter :
    androidx.recyclerview.widget.ListAdapter<StockItem, StoreItemsListAdapter.StoreCapsuleViewHolder>(DiffStoreCapsuleCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): StoreCapsuleViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.capsule_card, parent, false)

        return StoreCapsuleViewHolder(view)
    }

    override fun onBindViewHolder(holder: StoreCapsuleViewHolder, position: Int) {
        holder.bind(position)
    }

    inner class StoreCapsuleViewHolder(view: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(view){
        fun bind(position: Int) {
            val stockItem = getItem(position)

            itemView.txt_name.text = stockItem.capsule.name
            itemView.txt_price.text = itemView.context.getString(R.string.shekel_sign).plus(stockItem.capsule.price)
            itemView.txt_intensity.text = stockItem.capsule.intensity.toString()

            if (stockItem.capsule.itemPic == null) {
                itemView.img_capsule.setImageDrawable(ContextCompat.getDrawable(itemView.context, R.mipmap.ic_launcher))
            } else {
                itemView.img_capsule.setImageBitmap(stockItem.capsule.itemPic)
            }

            stockItem.capsule.brandPic?.let { itemView.img_brand.setImageBitmap(it) }

            itemView.setOnTouchListener(StoreCapsuleTouchListener(stockItem))
            (itemView.layoutParams as ViewGroup.MarginLayoutParams).setMargins(24, 24, 24, 0)
        }

        private inner class StoreCapsuleTouchListener(private val stockItem: StockItem): View.OnTouchListener {
            override fun onTouch(view: View?, motionEvent: MotionEvent?): Boolean {
                if (motionEvent?.action == MotionEvent.ACTION_DOWN) {
                    val data = ClipData.newPlainText("itemName", stockItem.capsule.name)
                    val shadowBuilder = View.DragShadowBuilder(view)

                    if (android.os.Build.VERSION.SDK_INT >= 24) {
                        view?.startDragAndDrop(data, shadowBuilder, view, 0)
                    } else {
                        view?.startDrag(data, shadowBuilder, view, 0)
                    }

                    return true
                }

                return false
            }
        }
    }
}

private class DiffStoreCapsuleCallback : DiffUtil.ItemCallback<StockItem>() {
    override fun areItemsTheSame(oldItem: StockItem, newItem: StockItem): Boolean {
        return oldItem.capsule.name == newItem.capsule.name
    }

    override fun areContentsTheSame(oldItem: StockItem, newItem: StockItem): Boolean {
        return oldItem == newItem
    }
}