package idf.viper.coffee_register.ui.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DiffUtil
import idf.viper.coffee_register.R
import idf.viper.coffee_register.model.Customer
import kotlinx.android.synthetic.main.user_card.view.*

open class CustomerListAdapter(private val customerListener: CustomerCardListener) :
    androidx.recyclerview.widget.ListAdapter<Customer, CustomerListAdapter.CustomerViewHolder>(DiffCustomerCallback()) {

    interface CustomerCardListener {
        fun onCustomerClick(customer: Customer)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomerViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.user_card, parent, false)

        return CustomerViewHolder(view)
    }

    override fun onBindViewHolder(holder: CustomerViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    inner class CustomerViewHolder(view: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(view){
        fun bind(customer: Customer) {
            itemView.txt_customer_name.text = customer.display_name

            if (customer.profilePic != null) {
                itemView.img_customer.setImageBitmap(customer.profilePic)
            } else {
                itemView.img_customer.setImageDrawable(ContextCompat.getDrawable(itemView.context, R.mipmap.ic_launcher))
            }

            itemView.setOnClickListener {
                customerListener.onCustomerClick(customer)
                it.setOnClickListener(null)
            }
        }
    }
}

private class DiffCustomerCallback : DiffUtil.ItemCallback<Customer>() {
    override fun areItemsTheSame(oldItem: Customer, newItem: Customer): Boolean {
        return oldItem.email == newItem.email
    }

    override fun areContentsTheSame(oldItem: Customer, newItem: Customer): Boolean {
        return oldItem == newItem
    }
}