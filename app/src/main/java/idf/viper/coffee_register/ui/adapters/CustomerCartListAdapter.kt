package idf.viper.coffee_register.ui.adapters

import android.view.DragEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import idf.viper.coffee_register.R
import idf.viper.coffee_register.model.SelectedCustomer
import idf.viper.coffee_register.model.StockItem
import kotlinx.android.synthetic.main.customer_cart_item.view.*
import kotlinx.android.synthetic.main.user_card.view.*

class CustomerCartListAdapter(private val cartListener: CustomersCartListener) :
    androidx.recyclerview.widget.ListAdapter<SelectedCustomer, CustomerCartListAdapter.CustomerCartViewHolder>(DiffCustomerCartCallback()) {

    interface CustomersCartListener {
        fun onCustomerCancelClick(selectedCustomer: SelectedCustomer)
        fun getItemByName(name: String) : StockItem?
    }

    override fun onBindViewHolder(holder: CustomerCartViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomerCartViewHolder {
        return CustomerCartViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.customer_cart_item, parent, false)
        )
    }

    inner class CustomerCartViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        fun bind(selectedCustomer: SelectedCustomer) {
            itemView.txt_customer_name.text = selectedCustomer.customer.display_name

            if (selectedCustomer.customer.profilePic == null) {
                itemView.img_customer.setImageDrawable(ContextCompat.getDrawable(itemView.context, R.mipmap.ic_launcher))
            } else {
                itemView.img_customer.setImageBitmap(selectedCustomer.customer.profilePic)
            }

            itemView.img_cancel.setOnClickListener {
                cartListener.onCustomerCancelClick(selectedCustomer)
                it.setOnClickListener(null)
            }

            val cartCapsAdapter = CartCapsuleListAdapter(selectedCustomer)
            itemView.rv_customer_capsules_cart.adapter = cartCapsAdapter
            itemView.setOnDragListener(CartDragListener(selectedCustomer, cartCapsAdapter))

            if (!selectedCustomer.purchase.capsules.isEmpty()) {
                cartCapsAdapter.submitList(selectedCustomer.purchase.capsules.toList())
            }
        }

        inner class CartDragListener(private val selectedCustomer: SelectedCustomer, private val adapter: CartCapsuleListAdapter) : View.OnDragListener {
            override fun onDrag(v: View, event: DragEvent): Boolean {
                val normalLayout = ContextCompat.getDrawable(v.context, R.drawable.cart_layout)
                val startLayout = ContextCompat.getDrawable(v.context, R.drawable.cart_drop_started)
                val enterLayout = ContextCompat.getDrawable(v.context, R.drawable.cart_drop_selected)

                when (event.action) {
                    DragEvent.ACTION_DRAG_STARTED -> v.background = startLayout
                    DragEvent.ACTION_DRAG_ENTERED -> v.background = enterLayout
                    DragEvent.ACTION_DRAG_EXITED -> v.background = startLayout
                    DragEvent.ACTION_DROP -> {
                        val stockItem = cartListener.getItemByName(event.clipData.getItemAt(0).text.toString())
                        stockItem?.let { stockItem ->
                            val selectedCap = stockItem.capsule.copy()
                            selectedCap.itemPic = stockItem.capsule.itemPic
                            selectedCap.brandPic = stockItem.capsule.brandPic
                            selectedCap.id = System.currentTimeMillis()

                            selectedCustomer.purchase.capsules.add(selectedCap)
                            selectedCustomer.purchase.quantity = selectedCustomer.purchase.capsules.size
                            selectedCustomer.purchase.price = selectedCustomer.purchase.capsules.sumByDouble { it.price }

                            adapter.submitList(selectedCustomer.purchase.capsules.toList())
                        }
                    }
                    DragEvent.ACTION_DRAG_ENDED -> v.background = normalLayout
                }

                return true
            }
        }
    }
}

private class DiffCustomerCartCallback : DiffUtil.ItemCallback<SelectedCustomer>() {
    override fun areItemsTheSame(oldItem: SelectedCustomer, newItem: SelectedCustomer): Boolean {
        return oldItem.customer.email == newItem.customer.email
    }

    override fun areContentsTheSame(oldItem: SelectedCustomer, newItem: SelectedCustomer): Boolean {
        return oldItem == newItem
    }
}