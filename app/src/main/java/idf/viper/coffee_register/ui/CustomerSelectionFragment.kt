package idf.viper.coffee_register.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import idf.viper.coffee_register.R
import idf.viper.coffee_register.model.Customer
import idf.viper.coffee_register.ui.adapters.CustomerListAdapter
import idf.viper.coffee_register.viewmodel.CustomerCartViewModel
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_customer_cart.*

class CustomerSelectionFragment : Fragment(), CustomerListAdapter.CustomerCardListener {
    private lateinit var customersAdapter: CustomerListAdapter
    private var customersCartVM: CustomerCartViewModel? = null

    override fun onCustomerClick(customer: Customer) {
        customersCartVM?.selectCustomer(customer)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        customersCartVM = activity?.run {
            ViewModelProviders.of(this).get(CustomerCartViewModel::class.java)
        }

        customersAdapter = CustomerListAdapter(this)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_customer_selection, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        rv_customer_selection.adapter = customersAdapter

        customersCartVM?.getAllCustomersLive()?.observe(viewLifecycleOwner, Observer { customers ->
            customersAdapter.submitList(customers?.toList())
        })
    }
}
