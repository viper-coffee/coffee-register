package idf.viper.coffee_register.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import idf.viper.coffee_register.R
import idf.viper.coffee_register.model.SelectedCustomer
import idf.viper.coffee_register.model.StockItem
import idf.viper.coffee_register.ui.adapters.CustomerCartListAdapter
import idf.viper.coffee_register.viewmodel.CustomerCartViewModel
import idf.viper.coffee_register.viewmodel.StockItemsViewModel
import kotlinx.android.synthetic.main.fragment_customer_cart.*

class CustomerCartFragment : Fragment(), CustomerCartListAdapter.CustomersCartListener {
    private var customersCartVM: CustomerCartViewModel? = null
    private var capsulesVM: StockItemsViewModel? = null
    private lateinit var customerCartAdapter : CustomerCartListAdapter

    override fun onCustomerCancelClick(selectedCustomer: SelectedCustomer) {
        customersCartVM?.deselectCustomer(selectedCustomer)
    }

    override fun getItemByName(name: String) : StockItem? {
        return capsulesVM?.getItemByName(name)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        customersCartVM = activity?.run {
            ViewModelProviders.of(this).get(CustomerCartViewModel::class.java)
        }

        capsulesVM = activity?.run {
            ViewModelProviders.of(this).get(StockItemsViewModel::class.java)
        }

        customerCartAdapter = CustomerCartListAdapter(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_customer_cart, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        rv_customer_selection.adapter = customerCartAdapter

        customersCartVM?.getSelectedCustomersLive()?.observe(viewLifecycleOwner, Observer { selectedList ->
            customerCartAdapter.submitList(selectedList)
        })
    }
}