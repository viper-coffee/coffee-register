package idf.viper.coffee_register.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import idf.viper.coffee_register.R
import idf.viper.coffee_register.ui.adapters.StoreItemsListAdapter
import idf.viper.coffee_register.viewmodel.StockItemsViewModel
import kotlinx.android.synthetic.main.fragment_capsule_store.*

class StoreFragment : Fragment() {
    private lateinit var storeItemsAdapter: StoreItemsListAdapter
    private var stockItemsVM: StockItemsViewModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        stockItemsVM = activity?.run {
            ViewModelProviders.of(this).get(StockItemsViewModel::class.java)
        }

        storeItemsAdapter = StoreItemsListAdapter()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_capsule_store, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        rv_capsules.adapter = storeItemsAdapter
        rv_capsules.layoutManager = GridLayoutManager(this.context, 3)

        stockItemsVM?.getAllStockItems()?.observe(viewLifecycleOwner, Observer { capsList ->
            storeItemsAdapter.submitList(capsList)
        })
    }
}
