package idf.viper.coffee_register.ui

import android.content.Context
import android.net.ConnectivityManager
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.findNavController
import idf.viper.coffee_register.R
import idf.viper.coffee_register.viewmodel.StockItemsViewModel
import kotlinx.android.synthetic.main.fragment_progress_bar.*

class ProgressBarFragment : Fragment() {

    private var stockItemsViewModel: StockItemsViewModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        stockItemsViewModel = activity?.run {
            ViewModelProviders.of(this).get(StockItemsViewModel::class.java)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_progress_bar, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val connectivityManager = context?.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetwork = connectivityManager.activeNetworkInfo

        stockItemsViewModel?.getAllStockItems()?.observe(viewLifecycleOwner, Observer { stockItems ->
            val stockItemsSize = stockItemsViewModel?.getStockItemsSize()?.value
            val isConnected = activeNetwork?.isConnected ?: false

            if ((stockItemsSize == null || stockItemsSize == 0) && !isConnected) {
                if (no_connection_gif.visibility == View.GONE) {
                    no_connection_gif.visibility = View.VISIBLE
                    no_connection_tv.visibility = View.VISIBLE
                    loading_gif.visibility = View.GONE
                    loading_tv.visibility = View.GONE
                }
            }
            else {
                if (loading_gif.visibility == View.GONE) {
                    loading_gif.visibility = View.VISIBLE
                    loading_tv.visibility = View.VISIBLE
                    no_connection_gif.visibility = View.GONE
                    no_connection_tv.visibility = View.GONE
                }
            }

            if (stockItemsSize != null && stockItemsSize != 0 && stockItems.size == stockItemsSize) {
                view.findNavController().navigate(R.id.mainFragment)
            }
        })
    }
}