package idf.viper.coffee_register.shared

object FirebaseCollections {
    const val USERS = "users"
    const val PURCHASES = "purchases"
    const val PURCHASE_CAPSULES = "capsules"
    const val BRANDS = "brands"
    const val STOCK_ITEMS = "stock"
}