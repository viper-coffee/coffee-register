package idf.viper.coffee_register.model

class SelectedCustomer (
    val customer: Customer,
    var purchase: Purchase = Purchase())