package idf.viper.coffee_register.model

import android.graphics.Bitmap
import com.google.firebase.firestore.Exclude
import com.google.firebase.firestore.IgnoreExtraProperties

@IgnoreExtraProperties
data class Customer(
    var display_name: String = "אספרסו שחור חזק",
    var email: String = "",
    var picture_url: String = ""
) {
    var profilePic : Bitmap? = null
        @Exclude get

    fun picFileName() = this.email.substringBefore("@")

    override fun equals(other: Any?): Boolean {
        return this.profilePic == (other as Customer).profilePic && super.equals(other)
    }
}