package idf.viper.coffee_register.model

data class StockItem (var capsule: Capsule = Capsule(),
                      var amount: Int = 0) {
    fun itemFileName() : String = this.capsule.brand.plus("_").plus(this.capsule.name)
}