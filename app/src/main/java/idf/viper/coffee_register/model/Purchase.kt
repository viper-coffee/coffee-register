package idf.viper.coffee_register.model

import com.google.firebase.firestore.Exclude
import com.google.firebase.firestore.IgnoreExtraProperties
import java.util.*
import kotlin.collections.ArrayList

@IgnoreExtraProperties
data class Purchase(
    var date: Date = Date(),
    var price: Double = 0.0,
    var quantity: Int = 0) {

    var capsules: ArrayList<Capsule> = arrayListOf()
        @Exclude get
}