package idf.viper.coffee_register.model

import com.google.firebase.firestore.Exclude
import com.google.firebase.firestore.IgnoreExtraProperties
import kotlinx.serialization.Serializable
import android.graphics.Bitmap
import kotlinx.serialization.Transient

@Serializable
@IgnoreExtraProperties
data class Capsule (
    var name: String = "",
    var brand: String = "",
    var price: Double = 0.0,
    var intensity: Int = 0,
    var picture_url: String = "",
    var brand_url: String = "") {

    var id: Long? = null
        @Exclude get

    @Transient
    var itemPic : Bitmap? = null
        @Exclude get

    @Transient
    var brandPic : Bitmap? = null
        @Exclude get

    override fun equals(other: Any?): Boolean {
        val otherCap = other as Capsule
        return this.name == otherCap.name && this.id == otherCap.id && super.equals(other)
    }
}