package idf.viper.coffee_register.repository

import android.content.Context
import android.graphics.BitmapFactory
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.google.firebase.firestore.*
import idf.viper.coffee_register.model.StockItem
import idf.viper.coffee_register.shared.FirebaseCollections
import java.io.File

class StockRepository(context: Context) : FirebaseBaseRepository(context) {
    private val stockReference = firestoreInstance.collection(FirebaseCollections.STOCK_ITEMS)
    private var stockItemsSize = MutableLiveData<Int>()
    private var stockItems = MutableLiveData<List<StockItem>>()

    init {
        stockItems.value = listOf()

        stockReference.addSnapshotListener(EventListener<QuerySnapshot> { result, ex ->
            if (ex != null) {
                return@EventListener
            }

            stockItemsSize.value = result?.size()
            updateCapsulesData(result)
        })
    }

    private fun updateCapsulesData(result: QuerySnapshot?) {
        result?.let {
            val changedStockList = stockItems.value!!.toMutableList()

            for (change in result.documentChanges) {
                val currItem = change.document.toObject(StockItem::class.java)

                when (change.type) {
                    DocumentChange.Type.ADDED -> {
                        changedStockList.add(currItem)
                        getItemPic(currItem, 0)
                    }
                    DocumentChange.Type.MODIFIED -> {
                        val modifyIndex = changedStockList.indexOfFirst { it.capsule.name == currItem.capsule.name }
                        changedStockList[modifyIndex] = currItem
                        getItemPic(currItem, 0)
                    }
                    DocumentChange.Type.REMOVED -> {
                        val removeIndex = changedStockList.indexOfFirst { it.capsule.name == currItem.capsule.name }
                        changedStockList.removeAt(removeIndex)
                        deleteLocalPic(currItem.itemFileName())
                    }
                }

                stockItems.value = changedStockList
            }

            changedStockList.groupBy { it.capsule.brand_url }.forEach {
                getBrandPic(it.key, it.value, 0)
            }
        }
    }

    private fun getItemPic(stockItem: StockItem, triesCount: Int) {
        val itemFileName = stockItem.itemFileName()
        val itemPicFile = File(localPath.plus(itemFileName))

        if (!isConnectedToInternet()) {
            if (itemPicFile.exists()) {
                stockItem.capsule.itemPic = BitmapFactory.decodeFile(itemPicFile.absolutePath)
                stockItems.value = stockItems.value
            }
        } else {
            itemPicFile.createNewFile()
            val capRef = storageInstance.reference.child(stockItem.capsule.picture_url)

            capRef.getFile(itemPicFile).addOnSuccessListener {
                stockItem.capsule.itemPic = BitmapFactory.decodeFile(itemPicFile.absolutePath)
                stockItems.value = stockItems.value
            }.addOnFailureListener {
                itemPicFile.delete()

                if (triesCount < PICTURE_MAX_GET_TRIES) {
                    Thread.sleep(100)
                    getItemPic(stockItem, triesCount + 1)
                }
            }
        }
    }

    private fun getBrandPic(brandURL: String, brandItems: List<StockItem>, triesCount: Int) {
        if (brandURL.isNotEmpty()) {
            val brandFileName = brandItems.first().capsule.brand
            val brandPicFile = File(localPath.plus(brandFileName))

            if (!isConnectedToInternet()) {
                if (brandPicFile.exists()) {
                    val brandPic = BitmapFactory.decodeFile(brandPicFile.absolutePath)
                    brandItems.forEach { item ->
                        item.capsule.brandPic = brandPic
                    }

                    stockItems.value = stockItems.value
                }
            } else {
                brandPicFile.createNewFile()

                val brandRef = storageInstance.reference.child(brandURL)

                brandRef.getFile(brandPicFile).addOnSuccessListener {
                    val brandPic = BitmapFactory.decodeFile(brandPicFile.absolutePath)
                    brandItems.forEach { item ->
                        item.capsule.brandPic = brandPic
                    }

                    stockItems.value = stockItems.value
                }.addOnFailureListener {
                    brandPicFile.delete()

                    if (triesCount < PICTURE_MAX_GET_TRIES) {
                        Thread.sleep(100)
                        getBrandPic(brandURL, brandItems, triesCount + 1)
                    }
                }
            }
        }
    }

    fun getStockItems(): LiveData<List<StockItem>> = stockItems

    fun getStockItemsSize(): LiveData<Int> = stockItemsSize

//    private fun firstDbInit() {
//        stockReference.apply {
//            add(StockItem(Capsule("חזק במיוחד","עלית",1.1,14,"stockItems/classic_14.png","brand_logos/elite_logo.png")))
//            add(StockItem(Capsule("אוגנדה", "עלית", 1.1, 13, "stockItems/uganda.png", "brand_logos/elite_logo.png")))
//            add(StockItem(Capsule("אינדונזיה","עלית",1.1,12,"stockItems/indonasia.png","brand_logos/elite_logo.png")))
//            add(StockItem(Capsule("גואטמלה","עלית",1.1,11,"stockItems/guatamala.png","brand_logos/elite_logo.png")))
//            add(StockItem(Capsule("אספרסו 10","עלית",1.1,10,"stockItems/classic_10.png","brand_logos/elite_logo.png")))
//            add(StockItem(Capsule("אספרסו 09","עלית",1.1,9,"stockItems/classic_09.png","brand_logos/elite_logo.png")))
//            add(StockItem(Capsule("קולומביה", "עלית", 1.1, 8, "stockItems/colombia.png", "brand_logos/elite_logo.png")))
//            add(StockItem(Capsule("אספרסו 07","עלית",1.1,7,"stockItems/classic_07.png","brand_logos/elite_logo.png")))
//            add(StockItem(Capsule("ברזיל", "עלית", 1.1, 6, "stockItems/brazil.png", "brand_logos/elite_logo.png")))
//            add(StockItem(Capsule("אגוזי לוז", "עלית", 1.1, 7, "stockItems/nuts.png", "brand_logos/elite_logo.png")))
//            add(StockItem(Capsule("וניל", "עלית", 1.1, 6, "stockItems/vanil.png", "brand_logos/elite_logo.png")))
//            add(StockItem(Capsule("קרמל", "עלית", 1.1, 7, "stockItems/caramel.png", "brand_logos/elite_logo.png")))
//            add(StockItem(Capsule("נטול קפאין","עלית",1.1,7,"stockItems/classic_natul_07.png","brand_logos/elite_logo.png")))
//        }
//    }
}