package idf.viper.coffee_register.repository

import android.content.Context
import android.graphics.*
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.google.firebase.firestore.DocumentChange
import com.google.firebase.firestore.EventListener
import com.google.firebase.firestore.QuerySnapshot
import idf.viper.coffee_register.model.Customer
import idf.viper.coffee_register.model.SelectedCustomer
import idf.viper.coffee_register.shared.FirebaseCollections
import java.io.File
import java.util.*


class CustomerRepository(context: Context) : FirebaseBaseRepository(context) {
    private val customersReference = firestoreInstance.collection(FirebaseCollections.USERS)
    private var customersSize = MutableLiveData<Int>()
    private var customers = MutableLiveData<List<Customer>>()

    init {

//        customersReference.get().addOnSuccessListener {
//            customersSize.value = it.size()
//        }
        customers.value = listOf()

        customersReference.addSnapshotListener(EventListener<QuerySnapshot> { result, ex ->
            if (ex != null) {
                return@EventListener
            }

            customersSize.value = result?.size()
            updateCustomersData(result)
        })
    }

    private fun updateCustomersData(result: QuerySnapshot?) {
        result?.let {
            val changedCustomersList = customers.value!!.toMutableList()

            for (change in result.documentChanges) {
                val currCustomer = change.document.toObject(Customer::class.java)

                when (change.type) {
                    DocumentChange.Type.ADDED -> {
                        changedCustomersList.add(currCustomer)
                        getUserPic(currCustomer, 0)
                    }
                    DocumentChange.Type.MODIFIED -> {
                        val index = changedCustomersList.indexOfFirst { it.email == currCustomer.email }
                        changedCustomersList[index] = currCustomer
                        getUserPic(currCustomer, 0)
                    }
                    DocumentChange.Type.REMOVED -> {
                        val removeIndex = changedCustomersList.indexOfFirst { it.email == currCustomer.email }
                        changedCustomersList.removeAt(removeIndex)
                        deleteLocalPic(currCustomer.picFileName())
                    }
                }

                customers.value = changedCustomersList
            }
        }
    }

    private fun getUserPic(customer: Customer, triesCount: Int) {
        val userFileName = customer.picFileName()
        val pictureFile = File(localPath.plus(userFileName))

        if (!isConnectedToInternet()) {
            if (pictureFile.exists()) {
                customer.profilePic = getCircularPicture(BitmapFactory.decodeFile(pictureFile.absolutePath))
                customers.value = customers.value
            }
        } else {
            pictureFile.createNewFile()
            val customerRef = storageInstance.getReference(customer.picture_url)

            customerRef.getFile(pictureFile).addOnSuccessListener {
                customer.profilePic = getCircularPicture(BitmapFactory.decodeFile(pictureFile.absolutePath))
                customers.value = customers.value
            }.addOnFailureListener {
                pictureFile.delete()

                if (triesCount < PICTURE_MAX_GET_TRIES) {
                    Thread.sleep(100)
                    getUserPic(customer, triesCount + 1)
                }
            }
        }
    }

    private fun getCircularPicture(bitmap: Bitmap): Bitmap {
        val output = Bitmap.createBitmap(bitmap.width, bitmap.height, Bitmap.Config.ARGB_8888)
        val canvas = Canvas(output)

        val color = -0xbdbdbe
        val paint = Paint()
        val rect = Rect(0, 0, bitmap.width, bitmap.height)

        paint.isAntiAlias = true
        canvas.drawARGB(0, 0, 0, 0)
        paint.color = color

        canvas.drawCircle(bitmap.width / 2f, bitmap.height / 2f, bitmap.width / 2f, paint)

        paint.xfermode = PorterDuffXfermode(PorterDuff.Mode.SRC_IN)
        canvas.drawBitmap(bitmap, rect, rect, paint)

        return output
    }

    fun getDbCustomers(): LiveData<List<Customer>> = customers

    fun getCustomersSize(): LiveData<Int> = customersSize

    fun finishPurchase(customersList: List<SelectedCustomer>) {
        val batch = firestoreInstance.batch()

        for (selectedCus in customersList) {
            val newPurchaseDoc =
                customersReference.document(selectedCus.customer.email).collection(FirebaseCollections.PURCHASES)
                    .document()
            selectedCus.purchase.date = Date()
            batch.set(newPurchaseDoc, selectedCus.purchase)

            for (capsule in selectedCus.purchase.capsules) {
                val newCapDoc = newPurchaseDoc.collection(FirebaseCollections.PURCHASE_CAPSULES).document()
                batch.set(newCapDoc, capsule)
            }
        }

        batch.commit()
    }
}