package idf.viper.coffee_register.repository

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.FirebaseFirestoreSettings
import com.google.firebase.storage.FirebaseStorage
import java.io.File

abstract class FirebaseBaseRepository(val context: Context) {
    protected companion object {
        const val PICTURE_MAX_GET_TRIES = 3
    }

    protected val firestoreInstance = FirebaseFirestore.getInstance()
    protected val storageInstance = FirebaseStorage.getInstance()
    protected val localPath = context.filesDir.path.plus("/")

    init {
        firestoreInstance.firestoreSettings = FirebaseFirestoreSettings.Builder()
            .setPersistenceEnabled(true)
            .build()
    }

    protected fun isConnectedToInternet(): Boolean {
        val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetwork = connectivityManager.activeNetworkInfo

        return (activeNetwork?.isConnected == true)
    }

    protected fun deleteLocalPic(fileName: String) {
        val pictureFile = File(localPath.plus(fileName))

        if (pictureFile.exists()) {
            pictureFile.delete()
        }
    }
}