package idf.viper.coffee_register.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.ViewModel
import idf.viper.coffee_register.model.StockItem
import idf.viper.coffee_register.repository.StockRepository

open class StockItemsViewModel(application: Application) : AndroidViewModel(application) {
    private var stockRepository = StockRepository(application)
    private var stockItemsForUI = MediatorLiveData<List<StockItem>>()

    init {
        stockItemsForUI.value = listOf()

        stockItemsForUI.addSource(stockRepository.getStockItems()) {
            stockItemsForUI.value = it.filter { stockItem ->
                stockItem.capsule.itemPic != null && stockItem.capsule.brandPic != null
            }.sortedByDescending { stockItem -> stockItem.capsule.intensity }
        }
    }

    fun getStockItemsSize(): LiveData<Int> = stockRepository.getStockItemsSize()

    fun getAllStockItems(): LiveData<List<StockItem>> = stockItemsForUI

    fun getItemByName(name: String): StockItem {
        return stockItemsForUI.value!!.single { it.capsule.name == name }
    }
}