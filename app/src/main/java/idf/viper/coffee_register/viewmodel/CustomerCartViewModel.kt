package idf.viper.coffee_register.viewmodel

import android.app.Application
import android.content.Context
import androidx.lifecycle.*
import idf.viper.coffee_register.model.Customer
import idf.viper.coffee_register.model.SelectedCustomer
import idf.viper.coffee_register.repository.CustomerRepository

class CustomerCartViewModel(application: Application) : AndroidViewModel(application) {
    private var customerRepository = CustomerRepository(application)
    private var selectedCustomersLive = MutableLiveData<List<SelectedCustomer>>()
    private var customersForUI = MediatorLiveData<List<Customer>>()

    init {
        initNewPurchase()

        customersForUI.apply {
            addSource(customerRepository.getDbCustomers()) {
                customersForUI.value = getCustomerForUI()
            }

            addSource(selectedCustomersLive) {
                customersForUI.value = getCustomerForUI()
            }
        }
    }

    private fun initNewPurchase() {
        selectedCustomersLive.value = listOf()
    }

    private fun getCustomerForUI(): List<Customer>? {
        return customerRepository.getDbCustomers().value?.filter { customer ->
            selectedCustomersLive.value?.find { selectedCustomer ->
                customer.email == selectedCustomer.customer.email
            } == null && (customer.profilePic != null)
        }?.sortedBy { it.display_name }
    }

    fun selectCustomer(customer: Customer) {
        val newList = selectedCustomersLive.value?.toMutableList()
        newList?.add(SelectedCustomer(customer))
        selectedCustomersLive.value = newList
    }

    fun deselectCustomer(selectedCustomer: SelectedCustomer) {
        val newList = selectedCustomersLive.value?.toMutableList()
        newList?.remove(selectedCustomer)
        selectedCustomersLive.value = newList
    }

    fun finishPurchase() {
        customerRepository.finishPurchase(selectedCustomersLive.value!!)
        initNewPurchase()
    }

    fun getCapsulesAmount() = selectedCustomersLive.value?.sumBy { it.purchase.capsules.size } ?: 0
    fun getCustomersSize(): LiveData<Int> = customerRepository.getCustomersSize()

    fun getSelectedCustomersLive(): LiveData<List<SelectedCustomer>> = selectedCustomersLive
    fun getAllCustomersLive(): LiveData<List<Customer>> = customersForUI
}